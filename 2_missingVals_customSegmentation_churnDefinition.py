
# coding: utf-8

# In[191]:


import pandas as pd
import numpy as np
import scipy
import random
from collections import Counter

import matplotlib.pyplot as plt
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')

from sklearn.neighbors import KNeighborsRegressor, KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.utils import class_weight
from sklearn import preprocessing


# ### Load the grouped dataset

# In[477]:


data = pd.read_csv('new_joined_data.csv')


# In[250]:


pd.set_option('display.max_columns', None)
data.head()


# In[251]:


data.sort_values('tot_sum_price', inplace=True, ascending=False)
data.reset_index(inplace=True, drop=True)
data.head(4)


# ### Look for missing values

# In[252]:


null_values = {}

for i in data.columns.values:
    if any(data.isnull()[i]):
        null_values[i] = data.isnull()[i].value_counts()
        
null_df = pd.DataFrame(null_values).T
null_df['index'] = null_df.index.values
null_df.reset_index(level=0, drop=True, inplace=True)
null_df = null_df.melt('index', var_name='T_F', value_name='NUM')
null_df


# In[385]:


sns.barplot(x=null_df.iloc[:,0], y=null_df.iloc[:,2], hue=null_df.iloc[:,1], )
plt.title("Presence of Null Values in Columns", fontsize=16)
plt.xlabel('Col Name', fontsize=14)
plt.ylabel('Number of Instances ', fontsize=14)
plt.xticks(fontsize=12)
plt.rcParams["figure.figsize"] = [12,8]
plt.legend(fontsize=12)
#plt.xticks(rotation=90)
sns.set_style('white')
sns.set_palette('PiYG', 2)
#plt.show()
plt.savefig('null_plot.jpg')


# ### using nearest values to impute missing values for most_recent_prod_category and most_spent_prod_category

# In[254]:


plt.subplot(2,1,1)
pl1 = data['most_recent_prod_category'].value_counts().plot(kind='bar', figsize=(10,4))
pl1.axes.get_xaxis().set_visible(False)

plt.subplot(2,1,2)
pl1 = data['most_spent_prod_category'].value_counts().plot(kind='bar', figsize=(10,4))
pl1.axes.get_xaxis().set_visible(False)


# In[255]:


data['most_recent_prod_category'] = data['most_recent_prod_category'].fillna(method='ffill')


# In[256]:


data['most_spent_prod_category'] = data['most_spent_prod_category'].fillna(method='ffill')


# In[257]:


plt.subplot(2,1,1)
pl1 = data['most_recent_prod_category'].value_counts().plot(kind='bar', figsize=(10,4))
pl1.axes.get_xaxis().set_visible(False)

plt.subplot(2,1,2)
pl1 = data['most_spent_prod_category'].value_counts().plot(kind='bar', figsize=(10,4))
pl1.axes.get_xaxis().set_visible(False)


# ### handling missing values in age 
# lot of missing values as zeros and also contains few number of nan

# In[258]:


data['age'].plot(kind='density')
plt.title('Age Distribution Before Imputation', fontsize=16)
plt.xlabel('Age', fontsize=12)
plt.ylabel('Density', fontsize=12)
plt.xticks(fontsize=11)
plt.yticks(fontsize=11)
plt.xlim(-10,80)
plt.savefig('age_before.jpg')

First impute nan with ffill and then we will deal with all the zero values
# In[259]:


data['age'].fillna(method='ffill', inplace=True)

KNN Regression to impute missing age values
# In[260]:


age_missing_x = data[data['age'] != 0][['tot_sum_price', 'txn_counts_unique', 'prod_counts_unique', 
                        'spent_per_trans', 'most_spent_prod_category']]
age_missing_x_predict = data[data['age'] == 0][['ID_user', 'tot_sum_price', 'txn_counts_unique', 'prod_counts_unique', 
                        'spent_per_trans', 'most_spent_prod_category']]
age_missing_y = data[data['age'] != 0][['age']]


age_knn = KNeighborsRegressor(n_neighbors=5, weights='distance', metric='euclidean')
age_knn.fit(age_missing_x, age_missing_y)
age_knn_missing_preds = age_knn.predict(age_missing_x_predict.iloc[:,1:])


# In[261]:


data.loc[data['ID_user'].isin(age_missing_x_predict['ID_user'].values), 'age'] = age_knn_missing_preds


# In[262]:


data['age'].plot('density')
plt.title('Age Distribution After Imputation', fontsize=16)
plt.xlabel('Age', fontsize=12)
plt.ylabel('Density', fontsize=12)
plt.xticks(fontsize=11)
plt.yticks(fontsize=11)
plt.xlim(-10,80)
plt.savefig('age_after.jpg')


# ### Dealing with missing gender

# In[276]:


data['gender'].value_counts().plot(kind='barh', figsize=(8,6))
plt.title("Distribution of genders BEFORE imputation", fontsize=16)
plt.xlabel('Count', fontsize=12)
plt.ylabel('', fontsize=12)
plt.xticks(fontsize=11)
plt.yticks(fontsize=11)
sns.set_palette('coolwarm')
plt.savefig('gender_before.jpg')

We will use the other variables to fill the unknown gender column using a simple Decision Tree Classifier
# In[277]:


gender_missing_x = data[data['gender'] != 'unknown'][['tot_sum_price', 'txn_counts_unique', 'prod_counts_unique', 
                        'spent_per_trans', 'most_spent_prod_category', 'age']]
gender_missing_x_predict = data[data['gender'] == 'unknown'][['ID_user', 'tot_sum_price', 'txn_counts_unique', 
                                                              'prod_counts_unique', 'spent_per_trans', 
                                                              'most_spent_prod_category', 'age']]
gender_missing_y = data[data['gender'] != 'unknown'][['gender']]



#compute class weights because the classes are a little unbalanced
class_weight_vals = class_weight.compute_class_weight('balanced', np.unique(gender_missing_y['gender']), 
                                                      gender_missing_y['gender'])
class_weight_dict = {'female':class_weight_vals[0], 'male':class_weight_vals[1]}
class_weight_dict


gender_tree = DecisionTreeClassifier(criterion='gini', class_weight=class_weight_dict)
gender_tree.fit(gender_missing_x, gender_missing_y)
gender_tree_missing_preds = gender_tree.predict(gender_missing_x_predict.iloc[:,1:])


# In[278]:


data.loc[data['ID_user'].isin(gender_missing_x_predict['ID_user'].values), 'gender'] = gender_tree_missing_preds


# In[279]:


data['gender'].value_counts().plot(kind='barh', figsize=(8,6))
plt.title("Distribution of genders AFTER imputation", fontsize=16)
plt.xlabel('Count', fontsize=12)
plt.ylabel('', fontsize=12)
plt.xticks(fontsize=11)
plt.yticks(fontsize=11)
sns.set_palette('coolwarm')
plt.savefig('gender_after.jpg')


# In[280]:


data.head()


# ### One hot encoding function

# In[281]:


def do_one_hot_encoding(df_name, df_column_name, suffix=''):
    x = pd.get_dummies(df_name[df_column_name])
    df_name = df_name.join(x, lsuffix=suffix)
    df_name = df_name.drop(df_column_name, axis=1) 
    return df_name


# ### Label encoding function

# In[282]:


def label_encoding_func(df_name, df_col_name):
    le = preprocessing.LabelEncoder()
    le.fit(df_name[df_col_name])
    return le.transform(df_name[df_col_name])


# ### get seasons from the dates
# manually define seasons

# In[283]:


from datetime import datetime, date

Y=2000
seasons = [('winter', (date(Y,  1,  1),  date(Y,  3, 20))),
           ('spring', (date(Y,  3, 21),  date(Y,  6, 20))),
           ('summer', (date(Y,  6, 21),  date(Y,  9, 22))),
           ('autumn', (date(Y,  9, 23),  date(Y, 12, 20))),
           ('winter', (date(Y, 12, 21),  date(Y, 12, 31)))]

def get_season(i):
    datetime_object = datetime.strptime(i, '%Y-%m-%d %H:%M:%S')
    date_object = datetime_object.date()
    #Y = get_year(i)
    date_object = date_object.replace(year=Y)
    return next(season for season, (start, end) in seasons if start <= date_object <= end)

def get_year(i):
    datetime_object = datetime.strptime(i, '%Y-%m-%d %H:%M:%S')
    return datetime_object.year


# In[284]:


data['first_trans_season'] = data['first_trans_date'].apply(get_season)
data['first_trans_season'] = label_encoding_func(data, 'first_trans_season')


# In[285]:


data['last_trans_season'] = data['last_trans_date'].apply(get_season)
data['last_trans_season'] = label_encoding_func(data, 'last_trans_season')


# Average number of days between transactions

# In[286]:


data['avg_days_bw_txns'] = round(data['days_bw_first_last_txn']/data['txn_counts_all'],2)


# In[287]:


data.head()

data.to_csv("new_joined_clean_data_1.csv", index=False)
#  

#  

# # Segmentation Models Start From Here

# In[288]:


cluster_data = pd.read_csv('new_joined_clean_data_1.csv')


# In[289]:


cluster_data.head()


# ### customer segmentation using k-means
# loosely based on recency, frequency and monetary measures of each customer

# In[31]:


from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import silhouette_score


# In[32]:


cluster_x_data = cluster_data[['ID_user','days_bw_first_last_txn', 'daysb/w_today_latest_txn', 'avg_days_bw_txns', 
                               'tot_sum_price', 'spent_per_trans', 'most_recent_trans_year', 'most_recent_trans_month', 
                              'txn_counts_unique', 'prod_counts_unique']]


# explore the data to be used for clustering

# In[312]:


sns.distplot(cluster_x_data['daysb/w_today_latest_txn'])
plt.title('Days between latest trasaction and Today (2015-05-04)', fontsize=16)
plt.xlabel('Number of Days', fontsize=12)
plt.ylabel('Density', fontsize=12)
plt.xticks(fontsize=11)
plt.yticks(fontsize=11)
sns.set_palette('coolwarm')
sns.set_style('white')
plt.savefig('days_bw_today_latest.jpg')

We can see that even if they have some new customers who recently made a purchase, a lot of their older customers have not come back to make a purchase
# In[332]:


sns.distplot(cluster_x_data['avg_days_bw_txns'])
plt.title('Average Days Between Transactions', fontsize=16)
plt.xlabel('Number of Days', fontsize=12)
plt.ylabel('Density', fontsize=12)
plt.xticks(fontsize=11)
plt.yticks(fontsize=11)
plt.xlim(-50, 200)
sns.set_palette('coolwarm')
sns.set_style('white')
plt.savefig('avg_days_bw_txn.jpg')


# In[375]:


sns.distplot(cluster_x_data['tot_sum_price'])
plt.title('Total Spending of Each Customer', fontsize=16)
plt.xlabel('Amount', fontsize=12)
plt.ylabel('Density', fontsize=12)
plt.xticks(fontsize=11)
plt.yticks(fontsize=11)
sns.set_palette('coolwarm')
sns.set_style('white')
plt.xlim(-1000, 250000)
plt.savefig('total_spending_each_customer.jpg')


# In[377]:


cluster_x_data['most_recent_trans_year'].value_counts().plot(kind='bar')
plt.title('Last transaction year of customers', fontsize=16)
plt.xlabel('Year', fontsize=12)
plt.xticks(rotation=360)
plt.ylabel('Count', fontsize=12)
plt.xticks(fontsize=11)
plt.yticks(fontsize=11)
sns.set_palette('YlOrRd')
plt.savefig('last_trans_year.jpg')


# In[361]:


cluster_x_data['most_recent_trans_month'].value_counts().plot(kind='bar')
plt.title('Month of latest transaction of customers', fontsize=16)
plt.xlabel('Month', fontsize=12)
plt.xticks(rotation=360)
plt.ylabel('Count', fontsize=12)
plt.xticks(fontsize=11)
plt.yticks(fontsize=11)
sns.set_palette('YlOrRd')
plt.savefig('last_trans_month.jpg')


# first scale the data and do k-means clustering

# In[38]:


std_scaler = StandardScaler()
cluster_x_scaled = pd.DataFrame(std_scaler.fit_transform(cluster_x_data.iloc[:, 1:]))
cluster_x_scaled.head()


# In[39]:


sum_squared_errors = {}
for i in range(2,7):
    cluster = KMeans(n_clusters=i).fit(cluster_x_scaled)
    label = cluster.labels_
    sil_coeff = silhouette_score(cluster_x_scaled, label, metric='euclidean')
    print("For n_clusters={}, The Silhouette Coefficient is {}".format(i, sil_coeff))


# In[40]:


cluster = KMeans(n_clusters=3, random_state=1)
cluster_x_data['cluster'] = cluster.fit_predict(cluster_x_scaled)
cluster_x_data.cluster.value_counts()


# In[41]:


cluster_x_data.head()


# Eplore the clustered data

# In[42]:


cluster_x_data_1 = cluster_x_data[cluster_x_data['cluster']==0]
cluster_x_data_2 = cluster_x_data[cluster_x_data['cluster']==1]
cluster_x_data_3 = cluster_x_data[cluster_x_data['cluster']==2]
#cluster_x_data_4 = cluster_x_data[cluster_x_data['cluster']==3]


# In[43]:


cluster_x_data_1.describe()


# In[44]:


cluster_x_data_2.describe()


# In[45]:


cluster_x_data_3.describe()


# Cluster Distribution

# Per transaction spending

# In[403]:


plt.rcParams["figure.figsize"] = [14,10]

plt.subplot(2,2,1)
sns.distplot(cluster_x_data_1['spent_per_trans'], hist=False, kde=True, label='Cluster 1', color='red')
sns.distplot(cluster_x_data_2['spent_per_trans'], hist=False, kde=True, label='Cluster 2', color='black')
sns.distplot(cluster_x_data_3['spent_per_trans'], hist=False, kde=True, label='Cluster 3', color='green')
#sns.distplot(cluster_x_data_4['spent_per_trans'], hist=False, kde=True, label='4')
plt.xlabel("Per Transaction Spending for each cluster", fontsize=12)
plt.legend(fontsize=12)
plt.xlim(-5000, 50000)

plt.subplot(2,2,2)
sns.distplot(cluster_x_data_1['tot_sum_price'], hist=False, kde=True, label='Cluster 1', color='red')
sns.distplot(cluster_x_data_2['tot_sum_price'], hist=False, kde=True, label='Cluster 2', color='black')
sns.distplot(cluster_x_data_3['tot_sum_price'], hist=False, kde=True, label='Cluster 3', color='green')
#sns.distplot(cluster_x_data_4['tot_sum_price'], hist=False, kde=True, label='4')
plt.xlabel("Total spending for each cluster", fontsize=12)
plt.legend(fontsize=12)
plt.xlim(-100000, 500000)
plt.ylim(0, 0.00004)

plt.subplot(2,2,3)
sns.distplot(cluster_x_data_1['avg_days_bw_txns'], hist=False, kde=True, label='Cluster 1', color='red')
sns.distplot(cluster_x_data_2['avg_days_bw_txns'], hist=False, kde=True, label='Cluster 2', color='black')
sns.distplot(cluster_x_data_3['avg_days_bw_txns'], hist=False, kde=True, label='Cluster 3', color='green')
#sns.distplot(cluster_x_data_4['avg_days_bw_txns'], hist=False, kde=True, label='4')
plt.xlabel("Average number of days between each transaction", fontsize=12)
plt.legend(fontsize=12)
plt.xlim(-50, 200)

plt.subplot(2,2,4)
sns.distplot(cluster_x_data_1['daysb/w_today_latest_txn'], hist=False, kde=True, label='Cluster 1', color='red')
sns.distplot(cluster_x_data_2['daysb/w_today_latest_txn'], hist=False, kde=True, label='Cluster 2', color='black')
sns.distplot(cluster_x_data_3['daysb/w_today_latest_txn'], hist=False, kde=True, label='Cluster 3', color='green')
plt.xlabel("Days between last transaction and today", fontsize=12)
plt.legend(fontsize=12)

plt.rcParams['figure.constrained_layout.use'] = True
plt.tight_layout()
plt.savefig('segmentation1.jpg')


# Total spending
sns.distplot(cluster_x_data_1['tot_sum_price'], hist=False, kde=True, label='1', color='red')
sns.distplot(cluster_x_data_2['tot_sum_price'], hist=False, kde=True, label='2', color='black')
sns.distplot(cluster_x_data_3['tot_sum_price'], hist=False, kde=True, label='3', color='blue')
#sns.distplot(cluster_x_data_4['tot_sum_price'], hist=False, kde=True, label='4')
plt.xlabel("Total spending for each cluster")
plt.xlim(-100000, 500000)
plt.ylim(0, 0.00004)
plt.show()
# Average days between transactions
sns.distplot(cluster_x_data_1['avg_days_bw_txns'], hist=False, kde=True, label='1', color='red')
sns.distplot(cluster_x_data_2['avg_days_bw_txns'], hist=False, kde=True, label='2', color='black')
sns.distplot(cluster_x_data_3['avg_days_bw_txns'], hist=False, kde=True, label='3', color='blue')
#sns.distplot(cluster_x_data_4['avg_days_bw_txns'], hist=False, kde=True, label='4')
plt.xlabel("Average number of days between each transaction")
plt.xlim(-50, 200)
plt.show()import matplotlib
colorname = []
colorid = []
for name, hex in matplotlib.colors.cnames.items():
    colorname.append(name)
    colorid.append(hex)
    
zippedcolors = list(zip(colorname, colorid))
zippedcolors = sorted(zippedcolors, key=lambda x: x[1])
zippedcolors
# Days between today and last transaction

# In[415]:


plt.title('Churn thresholds for each cluster', fontsize=16)
sns.distplot(cluster_x_data_1['daysb/w_today_latest_txn'], hist=True, kde=True, label='Cluster 1', color='red',
            hist_kws={"linewidth": 3,"alpha": 0.4})
plt.axvline(x=365, color='red', label="Threshold 1: 365")
sns.distplot(cluster_x_data_2['daysb/w_today_latest_txn'], hist=True, kde=True, label='Cluster 2', color='black',
            hist_kws={"linewidth": 3,"alpha": 0.4})
plt.axvline(x=300, color='black', label="Threshold 2: 300")
sns.distplot(cluster_x_data_3['daysb/w_today_latest_txn'], hist=True, kde=True, label='Cluster 3', color='green',
            hist_kws={"linewidth": 3,"alpha": 0.4})
plt.axvline(x=230, color='green', label="Threshold 3: 230")
plt.xlabel('Days between today and last transaction', fontsize=14)
plt.xticks(fontsize=10)
plt.yticks(fontsize=10)
plt.legend(fontsize=14)
plt.xlim(-100, 1000)
sns.set_context('paper')
plt.savefig('segmentation_churn_thres.jpg')


#  

#  

#  

# #### set labels for churn and join the dataset

# In[106]:


cluster_assignments = cluster_x_data[['ID_user', 'cluster']]


# In[159]:


NEW_DATA = cluster_data.merge(cluster_assignments, on='ID_user')


# In[160]:


NEW_DATA['churn'] = [0]*len(NEW_DATA)


# In[164]:


NEW_DATA.loc[(NEW_DATA['cluster'] == 0) & (NEW_DATA['daysb/w_today_latest_txn'] > 365), 'churn'] = 1


# In[165]:


NEW_DATA.loc[(NEW_DATA['cluster'] == 1) & (NEW_DATA['daysb/w_today_latest_txn'] > 300), 'churn'] = 1


# In[166]:


NEW_DATA.loc[(NEW_DATA['cluster'] == 2) & (NEW_DATA['daysb/w_today_latest_txn'] > 230), 'churn'] = 1


# Extract year of first transaction from date

# In[171]:


def convert_to_datetime_get_year(txn_time):
    datetime_object = datetime.strptime(txn_time, '%Y-%m-%d %H:%M:%S')
    return datetime_object.year


# In[172]:


NEW_DATA['first_trans_year'] = NEW_DATA['first_trans_date'].apply(convert_to_datetime_get_year)


# Save the new dataset as csv and use it for building models
NEW_DATA.to_csv('churn_labels_new_data.csv', index=False, sep=",")
# #### Plot Churn Label Distribution

# In[427]:


clus_df = pd.read_csv('churn_labels_new_data.csv')


# In[476]:


plt.rcParams["figure.figsize"] = [10,10]
plt.subplot(2,2,1)
plt.rcParams["figure.figsize"] = [8,8]
clus_df['churn'].value_counts().plot(kind='pie')
plt.title('Churn Distribution All Data', fontsize=16)
sns.set_palette('OrRd', 2)
plt.ylabel('')

plt.subplot(2,2,2)
plt.rcParams["figure.figsize"] = [8,8]
clus_df.loc[clus_df['cluster']==0, 'churn'].value_counts().plot(kind='pie')
plt.title('Churn Distribution for Cluster 1', fontsize=16)
sns.set_palette('OrRd', 2)
plt.ylabel('')

plt.subplot(2,2,3)
plt.rcParams["figure.figsize"] = [8,8]
clus_df.loc[clus_df['cluster']==1, 'churn'].value_counts().plot(kind='pie')
plt.title('Churn Distribution for Cluster 2', fontsize=16)
sns.set_palette('OrRd', 2)
plt.ylabel('')

plt.subplot(2,2,4)
plt.rcParams["figure.figsize"] = [8,8]
clus_df.loc[clus_df['cluster']==2, 'churn'].value_counts().plot(kind='pie')
plt.title('Churn Distribution for Cluster 3', fontsize=16)
sns.set_palette('OrRd', 2)
plt.ylabel('')

plt.tight_layout()
plt.savefig('churn_dist.jpg')


# In[455]:


plt.rcParams["figure.figsize"] = [8,8]
clus_df.loc[clus_df['cluster']==0, 'churn'].value_counts().plot(kind='pie')
plt.title('Churn Distribution for Cluster 1', fontsize=16)
plt.legend(fontsize=12)
sns.set_palette('OrRd', 2)
plt.ylabel('')
plt.savefig('churn_dist1.jpg')

