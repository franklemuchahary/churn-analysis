
# coding: utf-8

# In[2]:


import pandas as pd
import numpy as np

#plotting libraries
import matplotlib.pyplot as plt
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')

#datetime
from datetime import datetime


# In[18]:


transactions = pd.read_csv('transactions.txt', sep="|")
transactions.head()


# In[19]:


users = pd.read_csv('users.txt', sep="|")
users.head()


# In[20]:


products = pd.read_csv('products.txt', sep="|")
products.head()


# In[6]:


print(products.drop_duplicates().shape)


# In[7]:


print(users.drop_duplicates().shape)


# In[8]:


print(transactions.drop_duplicates().shape)


# #### joins

# In[9]:


trans_prods_df = pd.merge(transactions, products, left_on='ID_product', right_on='ID_product', how='left')


# In[10]:


users_trans_prods_df = pd.merge(trans_prods_df, users, left_on='ID_user', right_on='ID_user', how='inner')


# In[11]:


users_trans_prods_df.head()


# In[12]:


def convert_to_datetime(txn_time):
    datetime_object = datetime.strptime(txn_time, '%Y-%m-%d %H:%M:%S')
    return datetime_object

users_trans_prods_df['txn_time'] = users_trans_prods_df['txn_time'].apply(convert_to_datetime)


# #### number of days between first and last transaction for each user

# In[13]:


txn1_txnlast_days = users_trans_prods_df.groupby(by='ID_user')[['txn_time']].agg(np.ptp)
txn1_txnlast_days.reset_index(inplace=True)

txn1_txnlast_days['days_bw_first_last_txn'] = txn1_txnlast_days['txn_time'].apply(lambda x: x.days) 

txn1_txnlast_days.head()


# #### days between latest transaction and today
# today is the latest date of transaction in the whole database

# In[14]:


today = max(users_trans_prods_df['txn_time'])
days_bw_latest_today_txn = users_trans_prods_df.groupby(by='ID_user')['txn_time'].agg(['max', 'min'])
days_bw_latest_today_txn.reset_index(inplace=True)

days_bw_latest_today_txn['daysb/w_today_latest_txn'] = days_bw_latest_today_txn['max'].apply(lambda x: (today-x).days)

days_bw_latest_today_txn['first_trans_date'] = days_bw_latest_today_txn['min']
days_bw_latest_today_txn['last_trans_date'] = days_bw_latest_today_txn['max']
days_bw_latest_today_txn.drop(['max', 'min'], inplace=True, axis=1)
days_bw_latest_today_txn.head()


# In[17]:


today


# #### count unique of number of transactions

# In[167]:


count_txns = users_trans_prods_df.groupby(by='ID_user')[['ID_txn', 'ID_category']].nunique()
count_txns.reset_index(inplace=True)
count_txns['txn_counts_unique'] = count_txns['ID_txn']
count_txns['prod_counts_unique'] = count_txns['ID_category']
count_txns.drop(['ID_txn', 'ID_category'], inplace=True, axis=1)
count_txns.head()


# #### count total number of transactions for each user

# In[203]:


count_txns_user = users_trans_prods_df.groupby(by='ID_user')[['ID_txn']].count()
count_txns_user.reset_index(inplace=True)
count_txns_user['txn_counts_all'] = count_txns_user['ID_txn']
count_txns_user.drop(['ID_txn'], inplace=True, axis=1)
count_txns_user.head()


# #### total price spent by each user

# In[170]:


tot_sum_price = users_trans_prods_df.groupby(by='ID_user')[['price']].sum()
tot_sum_price.reset_index(inplace=True)
tot_sum_price['tot_sum_price'] = tot_sum_price['price']
tot_sum_price.drop(['price'], inplace=True, axis=1)
tot_sum_price.head()


# #### most spent product category

# In[282]:


temp = users_trans_prods_df.groupby(by='ID_user')[['price']].max()
temp.reset_index(inplace=True)
most_spent_prod_category = pd.merge(temp, users_trans_prods_df, left_on=['ID_user', 'price'], 
         right_on=['ID_user', 'price'], how='inner').drop_duplicates().reset_index()[['ID_user', 'ID_category']]
most_spent_prod_category['most_spent_prod_category'] = most_spent_prod_category['ID_category']
most_spent_prod_category.drop('ID_category', inplace=True, axis=1)
most_spent_prod_category = most_spent_prod_category.groupby('ID_user').first()
most_spent_prod_category.reset_index(inplace=True)


# #### most recent spent product category

# In[281]:


temp = users_trans_prods_df.groupby(by='ID_user')[['txn_time']].max()
temp.reset_index(inplace=True)
most_recent_prod_category = pd.merge(temp, users_trans_prods_df, left_on=['ID_user', 'txn_time'], 
         right_on=['ID_user', 'txn_time'], how='inner').drop_duplicates().reset_index()[['ID_user', 'ID_category']]
most_recent_prod_category['most_recent_prod_category'] = most_recent_prod_category['ID_category']
most_recent_prod_category.drop('ID_category', inplace=True, axis=1)
most_recent_prod_category = most_recent_prod_category.groupby('ID_user').first()
most_recent_prod_category.reset_index(inplace=True)


# #### join everything together

# In[283]:


new_u_t_p_df = users_trans_prods_df.groupby('ID_user')[['gender', 'age']].last()
new_u_t_p_df.reset_index(inplace=True)
new_u_t_p_df.head()


# In[284]:


new_u_t_p_df = pd.merge(new_u_t_p_df, txn1_txnlast_days, left_on='ID_user', right_on='ID_user', how='left')
new_u_t_p_df = pd.merge(new_u_t_p_df, days_bw_latest_today_txn, left_on='ID_user', right_on='ID_user', how='left')
new_u_t_p_df = pd.merge(new_u_t_p_df, count_txns, left_on='ID_user', right_on='ID_user', how='left')
new_u_t_p_df = pd.merge(new_u_t_p_df, count_txns_user, left_on='ID_user', right_on='ID_user', how='left')
new_u_t_p_df = pd.merge(new_u_t_p_df, tot_sum_price, left_on='ID_user', right_on='ID_user', how='left')
new_u_t_p_df = pd.merge(new_u_t_p_df, most_spent_prod_category, left_on='ID_user', right_on='ID_user', how='left')
new_u_t_p_df = pd.merge(new_u_t_p_df, most_recent_prod_category, left_on='ID_user', right_on='ID_user', how='left')


# In[287]:


new_u_t_p_df.head()


# In[288]:


new_u_t_p_df['spent_per_trans'] = new_u_t_p_df['tot_sum_price']/new_u_t_p_df['txn_counts_all']


# In[289]:


new_u_t_p_df['most_recent_trans_month'] = new_u_t_p_df['last_trans_date'].apply(lambda x: x.month)
new_u_t_p_df['most_recent_trans_year'] = new_u_t_p_df['last_trans_date'].apply(lambda x: x.year)


# In[290]:


new_u_t_p_df.shape


# #### save to csv

# In[291]:


new_u_t_p_df.to_csv('new_joined_data.csv', index=False)


# In[292]:


pd.read_csv('new_joined_data.csv')


# #### See the distribution of missing values

# In[239]:


null_values = {}

for i in new_u_t_p_df.columns.values:
    if any(new_u_t_p_df.isnull()[i]):
        null_values[i] = new_u_t_p_df.isnull()[i].value_counts()
        
null_df = pd.DataFrame(null_values).T
null_df['index'] = null_df.index.values
null_df.reset_index(level=0, drop=True, inplace=True)
null_df = null_df.melt('index', var_name='T_F', value_name='NUM')
null_df


# In[241]:


sns.barplot(x=null_df.iloc[:,0], y=null_df.iloc[:,2], hue=null_df.iloc[:,1])
plt.title("NULL VALUES")
plt.xlabel('Col Name')
plt.ylabel('Number of Instances ')
plt.rcParams["figure.figsize"] = [14,8]
plt.xticks(rotation=90)
plt.show()

