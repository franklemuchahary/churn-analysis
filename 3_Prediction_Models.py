
# coding: utf-8

# In[116]:


import pandas as pd
import numpy as np
import math

#plotting libraries
import matplotlib.pyplot as plt
import seaborn as sns

get_ipython().run_line_magic('matplotlib', 'inline')


# #### read the dataset

# In[557]:


pd.set_option('display.max_columns', None)

df = pd.read_csv('churn_labels_new_data.csv')


# In[43]:


df.head()


# In[44]:


df.describe()


# #### Utility functions

# In[134]:


#one hot encoder function
def do_one_hot_encoding(df_name, df_column_name, suffix=''):
    '''
    usage: dataframe[column_name] = do_one_hot_encoding(dataframe, column_name, suffix_for_column_name)
    '''
    x = pd.get_dummies(df_name[df_column_name])
    df_name = df_name.join(x, lsuffix=suffix)
    df_name = df_name.drop(df_column_name, axis=1) 
    return df_name


#label encoder function
def label_encoding_func(df_name, df_col_name):
    '''
    usage: dataframe[column_name] = label_encoding_function(dataframe, column_name)
    '''
    le = preprocessing.LabelEncoder()
    le.fit(df_name[df_col_name])
    return le.transform(df_name[df_col_name])


#draw confusion matrix
def draw_confusion_matrix(true,preds):
    conf_matx = metrics.confusion_matrix(true, preds)
    sns.heatmap(conf_matx, annot=True, annot_kws={"size": 12},fmt='g', cbar=True)
    plt.show()
    return conf_matx


#draw roc curve and give auc
def give_auc_draw_roc(test_values, prediction_values):
    fpr, tpr, thresholds = metrics.roc_curve(test_values, prediction_values)
    auc = metrics.auc(fpr, tpr)
    plt.plot(fpr,tpr)
    plt.xlabel('False Positive')
    plt.ylabel('True Positive')
    plt.title('ROC Curve, AUC=%s'%(auc))
    sns.set_style("darkgrid")
    plt.show()
    #plt.savefig('roc_auc.png')
    #return auc   
    #auc = np.trapz(true_pos,false_pos)
    #return auc


# #### Corrplot to see data leakage
# remove columns with too high correlation with the churn column

# In[239]:


sns.heatmap(df.corr(), annot=True, linewidths=1)
plt.rcParams["figure.figsize"] = [16,14]
plt.savefig('corr_plot.jpg')


# #### Remove unnecessary columns and label encode or one hot encode variables

# Remove unwanted columns

# In[189]:


new_df = df.drop(['daysb/w_today_latest_txn', 'most_recent_trans_year', 'txn_time', 
                  'first_trans_date', 'last_trans_date', 'first_trans_year'], axis=1)


# In[190]:


new_df.shape[1]


# One hot encode

# In[191]:


new_df = do_one_hot_encoding(new_df, 'gender', '_gender')


# In[192]:


new_df = do_one_hot_encoding(new_df, 'cluster', '_cluster')

new_df = do_one_hot_encoding(new_df, 'first_trans_year', '_first_trans_year')
# In[193]:


new_df.shape[1]


# #### Separate dataset into X and Y

# In[194]:


X = new_df[new_df.columns.difference(['ID_user', 'churn'])]


# In[195]:


Y = new_df['churn']


# #### Feature Scaling

# In[196]:


from sklearn.preprocessing import StandardScaler

scaler = StandardScaler()
X_scaled = scaler.fit_transform(X)


# #### Train Test Split

# In[197]:


train_x, test_x, train_y, test_y = cross_validation.train_test_split(X_scaled, Y, train_size=0.75, random_state=333,
                                                                    stratify=Y)


# #### Models

# In[253]:


from sklearn.grid_search import GridSearchCV
from sklearn import cross_validation
from sklearn import metrics

from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from xgboost import XGBClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import BaggingClassifier

from lifelines import CoxPHFitter


# #### create dict for class weights
# This is for dealing with imbalanced classes

# In[199]:


from sklearn.utils import class_weight
class_weight = class_weight.compute_class_weight('balanced', np.unique(train_y), train_y)
class_weight_dict = {0:class_weight[0], 1:class_weight[1]}
class_weight_dict


# #### feature importance study using random forests

# In[201]:


rf_feature_imp = RandomForestClassifier(max_depth=15, n_estimators=500)
rf_feature_imp = rf_feature_imp.fit(train_x, train_y)

features = pd.DataFrame()
features['feature'] = X.columns.values
features['importance'] = rf_feature_imp.feature_importances_
features.sort_values(by=['importance'], ascending=True, inplace=True)
features.set_index('feature', inplace=True)


features.plot(kind='barh', figsize=(20, 20))
plt.show()


# #### Random Forest

# In[202]:


#Random Forest Classifier
params = {'bootstrap':[True], 'max_depth':[5, 10, 13, 15], 'min_samples_leaf':np.logspace(0.1,3,3, dtype=int)}

rf = GridSearchCV(RandomForestClassifier(class_weight=class_weight_dict, n_estimators=1000), params, n_jobs=-1, cv=3, 
                   scoring='roc_auc', verbose=1)
rf = rf.fit(train_x, train_y)

print(rf.best_params_, rf.best_score_)

{'bootstrap': True, 'max_depth': 13, 'min_samples_leaf': 1} 0.798850439521477
# In[228]:


#make predictions 
rf_preds_proba = rf.predict_proba(test_x)
rf_preds = rf.predict(test_x)
rf_preds_train = rf.predict_proba(train_x)

#confusion matrix for random forest on test data
plt.rcParams["figure.figsize"] = [7,5]
rf_conf_matx = draw_confusion_matrix(test_y, rf_preds)
give_auc_draw_roc(test_y, rf_preds)


# #### Logistic Regression

# In[204]:


#logistic regression classifier
params = {'penalty':['l2'], 'C':np.logspace(0.01, 2, 10), 'solver':['lbfgs', 'newton-cg']}

lr = GridSearchCV(LogisticRegression(class_weight=class_weight_dict), params, n_jobs=-1, cv=5, scoring='roc_auc', verbose=1)
lr = lr.fit(train_x, train_y)

print(lr.best_params_, lr.best_score_)

{'C': 1.023292992280754, 'penalty': 'l2', 'solver': 'lbfgs'} 0.7384121398690396
# In[227]:


lr_preds = lr.predict(test_x)
lr_preds_proba = lr.predict_proba(test_x)
lr_preds_train = lr.predict_proba(train_x)

draw_confusion_matrix(test_y, lr_preds)
give_auc_draw_roc(test_y, lr_preds)


# #### Bagging Classifier

# In[222]:


#Bagging Classifier
params = {'bootstrap':[True]}

bc = GridSearchCV(BaggingClassifier(n_estimators=700), params, n_jobs=-1, cv=3, scoring='roc_auc', verbose=1)
bc = bc.fit(train_x, train_y)

print(bc.best_params_, bc.best_score_) 


# In[226]:


bc_preds = bc.predict(test_x)
bc_preds_train = bc.predict_proba(train_x)
bc_preds_proba = bc.predict_proba(test_x)


# In[224]:


draw_confusion_matrix(test_y, bc_preds)
give_auc_draw_roc(test_y, bc_preds)


# #### support vector

# In[237]:


# svm
params = {'C': np.linspace(1000, 2000, 5) ,'gamma': np.linspace(0.001, 5, 5), 'kernel': ['rbf'], 'probability':[True]}

svc = GridSearchCV(SVC(class_weight=class_weight_dict), params, n_jobs = -1, cv=3, scoring = 'roc_auc', 
                   verbose=1)
svc = svc.fit(train_x, train_y)
svc_preds_proba = svc.predict(test_x)


print("Best C & associated score", svc.best_params_, svc.best_score_)


# In[235]:


svc_preds_train = svc.predict(train_x)

Best C & associated score {'C': 1750.0, 'gamma': 0.001, 'kernel': 'rbf'} 0.7461014304048544svc_preds

draw_confusion_matrix(test_y, svc_preds)
give_auc_draw_roc(test_y, svc_preds)
# #### XGBoost

# In[215]:


#Xgboost
params =  { 'learning_rate':[0.1], 'n_estimators':[700], 'scale_pos_weight':[1.9, 0.5, 1], 
           'max_depth':[10, 5, 15], 'colsample_bytree':[0.7]
          }

xgb = GridSearchCV(XGBClassifier(), params, n_jobs=-1, cv=3, scoring='roc_auc', verbose=1)
xgb = xgb.fit(train_x, train_y)


print("Best C & associated score", xgb.best_params_, xgb.best_score_)


# In[251]:


xgb_preds_proba = xgb.predict_proba(test_x)
xgb_preds_train = xgb.predict_proba(train_x)
xgb_preds = xgb.predict(test_x)

draw_confusion_matrix(test_y, xgb_preds)
give_auc_draw_roc(test_y, xgb_preds)
plt.rcParams["figure.figsize"] = [10,8]

#extra trees classifier
params = {'criterion': ['entropy']}
etc = GridSearchCV(ExtraTreesClassifier(n_estimators = 1000, class_weight=class_weight_dict), params, 
                   n_jobs = -1, cv=3, scoring = 'roc_auc')
etc.fit(train_x, train_y)
etc_preds = etc.predict(test_x)

print("Best params & associated score", etc.best_params_, etc.best_score_)draw_confusion_matrix(test_y, etc_preds)
give_auc_draw_roc(test_y, etc_preds)
# #### Adaboost

# In[217]:


#ada boost classifier
params = {'learning_rate': [0.1]}
abc = GridSearchCV(AdaBoostClassifier(n_estimators = 700), params, n_jobs = -1, cv=3, scoring = 'roc_auc', verbose=1)
abc.fit(train_x, train_y)

print("Best params & associated score", abc.best_params_, abc.best_score_)


# In[246]:


abc_preds = abc.predict(test_x)
abc_preds_proba = abc.predict_proba(test_x)
abc_preds_train = abc.predict_proba(train_x)


# In[219]:


draw_confusion_matrix(test_y, abc_preds)
give_auc_draw_roc(test_y, abc_preds)


# ## Stacking

# In[252]:


'''
new_X_train = pd.DataFrame({
    'rf': rf_preds_train[:,0],
    'lr': lr_preds_train[:,0],
    'xgb': xgb_preds_train[:,0],
    'abc': abc_preds_train[:,0],
    'bc': bc_preds_train[:,0]
})
'''

new_X_test = pd.DataFrame({
    'rf': rf_preds_proba[:,0],
    'lr': lr_preds_proba[:,0],
    'xgb': xgb_preds_proba[:,0],
    'abc': abc_preds_proba[:,0],
    'bc': bc_preds_proba[:,0]
})


# In[310]:


train_x_2, test_x_2, train_y_2, test_y_2 = cross_validation.train_test_split(new_X_test, test_y, train_size=0.8, 
                                                                             stratify=test_y) 


# Logistic Regression Model for Ensemble

# In[333]:


#params = {'penalty':['l2'], 'C':[3], 'solver':['lbfgs', 'newton-cg']}

#ensembled_model_logit = GridSearchCV(LogisticRegression(), params, n_jobs=-1, cv=3, verbose=1, scoring='roc_auc')
ensembled_model_logit = LogisticRegression()
ensembled_model_logit.fit(train_x_2, train_y_2)

#print("Best params & associated score", ensembled_model_logit.best_params_, ensembled_model_logit.best_score_)


# In[375]:


ensemble_preds = ensembled_model_logit.predict(test_x_2)
ensemble_preds_proba = ensembled_model_logit.predict_proba(test_x_2)

ensemble_preds_train = ensembled_model_logit.predict(train_x_2)


# Cross Validated Scores

# In[378]:


scores = cross_validation.cross_val_score(ensembled_model_logit, test_x_2, test_y_2, cv=5, scoring='precision')
scores2 = cross_validation.cross_val_score(ensembled_model_logit, test_x_2, test_y_2, cv=5, scoring='recall')
scores3 = cross_validation.cross_val_score(ensembled_model_logit, test_x_2, test_y_2, cv=5, scoring='roc_auc')

print("\033[1mCROSS VALIDATED SCORES ON TEST SET \033[0m", end="\n\n")
print("ROC AUC SCORE: ", round(np.mean(scores3), 4), end="\n\n")
print("PRECISION SCORE: ", round(np.mean(scores), 4), end="\n\n")
print("RECALL SCORE: ", round(np.mean(scores2), 4))


# Confusion Matrix Plot

# In[380]:


import itertools
from pylab import savefig
def plot_confusion_matrix(cm, classes,
                          normalize=True,
                          title='Confusion matrix',
                          cmap=plt.cm.BuPu):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    np.set_printoptions(precision=2)
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    #plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)
    

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
        
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt


# In[367]:


conf_matrix = metrics.confusion_matrix(ensemble_preds, test_y_2)
plot_confusion_matrix(conf_matrix, ['Not Churn', 'Churn'])
plt.show()
conf_matx_fig = plt.gcf()
conf_matx_fig.savefig('conf_matx.jpg', dpi=100)


# Plot Precision Recall Curve

# In[382]:


from sklearn.metrics import precision_recall_curve
from sklearn.metrics import average_precision_score
from itertools import cycle

y_score = ensembled_model_logit.fit(test_x_2, test_y_2).decision_function(test_x_2)

print(y_score)
colors = cycle(['darkorange', 'cornflowerblue', 'teal'])
precision = dict()
recall = dict()
average_precision = dict()
for i in range(2):
    precision[i], recall[i], _ = precision_recall_curve(test_y_2,
                                                        ensemble_preds)
    average_precision[i] = average_precision_score(ensemble_preds, test_y_2)
 

    average_precision["micro"] = average_precision_score(test_y_2, y_score,
                                                     average="micro")


lw = 2
plt.clf()
plt.plot(recall[0], precision[0], lw=lw, color='navy',
         label='Precision-Recall curve')
plt.xlabel('Recall')
plt.ylabel('Precision')
plt.ylim([0.0, 1.05])
plt.xlim([0.0, 1.0])
plt.title('Precision-Recall Curve')
plt.legend(loc="lower left")
plt.savefig('PR-Curve.jpg')


#  

#  

#  

# # Survival analysis

# In[413]:


from lifelines.datasets import load_rossi
from lifelines import CoxPHFitter
from sklearn import preprocessing


# In[302]:


rossi_dataset = load_rossi()
cph = CoxPHFitter()
cph.fit(rossi_dataset, duration_col='week', event_col='arrest', show_progress=True)


# In[434]:


new_df_survival = df.drop(['most_recent_trans_year', 'txn_time', 
                  'first_trans_date', 'last_trans_date', 'first_trans_year'], axis=1)


# In[435]:


new_df_survival.head()


# In[436]:


new_df_survival.shape


# In[437]:


new_df_survival['gender'] = label_encoding_func(new_df_survival, 'gender')


# In[419]:


cph = CoxPHFitter()
cph.fit(new_df_survival, duration_col='daysb/w_today_latest_txn', event_col='churn', show_progress=True)


# In[438]:


cph2 = CoxPHFitter()
cph2.fit(new_df_survival, duration_col='daysb/w_today_latest_txn', event_col='churn', show_progress=True)


# In[439]:


cph2.print_summary()


# In[440]:


X = new_df_survival.drop(["daysb/w_today_latest_txn", "churn"], axis=1)


# In[444]:


X_cluster1 = X[X['cluster']==0]
X_cluster2 = X[X['cluster']==1]
X_cluster3 = X[X['cluster']==2]


# In[453]:


cluster3_survivals = cph2.predict_survival_function(X_cluster3)


# In[454]:


cluster2_survivals = cph2.predict_survival_function(X_cluster2)


# In[455]:


cluster1_survivals = cph2.predict_survival_function(X_cluster1)

value_list_clus3 = []
for i in range(0, cluster3_survivals.shape[1]):
    k=0;
    for j in cluster3_survivals.iloc[:, i]:
        if(j<0.6):
            value_list_clus3.append(k)
            break
        k+=1value_list_clus2 = []
for i in range(0, cluster2_survivals.shape[1]):
    k=0;
    for j in cluster2_survivals.iloc[:, i]:
        if(j<0.6):
            value_list_clus2.append(k)
            break
        k+=1
# In[556]:


temp = cph2.predict_percentile(X, p=0.5)
temp.plot(kind='density')
plt.title('50% Churn Probability', fontsize=14)
plt.xlabel('Days', fontsize=12)
plt.savefig("churn_days.jpg")


# Survival Function for 10 users from each cluster

# In[471]:


cluster3_survivals.iloc[:, 0:5].plot()
plt.title('Survival Function for 5 people from Cluster 3', fontsize=16)
plt.xlabel('Days', fontsize=12)
plt.ylabel('Probability', fontsize=12)
sns.set_style('white')
plt.savefig('survival1.jpg')


# In[472]:


cluster2_survivals.iloc[:, 0:5].plot()
plt.title('Survival Function for 5 people from Cluster 2', fontsize=16)
plt.xlabel('Days', fontsize=12)
plt.ylabel('Probability', fontsize=12)
sns.set_style('white')
plt.savefig('survival2.jpg')


# In[473]:


cluster1_survivals.iloc[:, 0:5].plot()
plt.title('Survival Function for 5 people from Cluster 1', fontsize=16)
plt.xlabel('Days', fontsize=12)
plt.ylabel('Probability', fontsize=12)
sns.set_style('white')
plt.savefig('survival3.jpg')

